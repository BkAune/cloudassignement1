package main

import (
	"testing"
)

func TestParseURL(t *testing.T) {
	//inputURL := "/projectinfo/v1/github.com/syoyo/tinyobjloader"
	inputURL := []string{"", "projectinfo", "v1", "github.com", "syoyo", "tinyobjloader"}
	expectedOutputRepoURL := "https://api.github.com/repos/syoyo/tinyobjloader"
	expectedOutputLanguageURL := expectedOutputRepoURL + "languages"
	expectedOutputContributorURL := expectedOutputRepoURL + "contributors"
	outputRepoURL, outputLanguageURL, outputContributorURL := ParseURL(inputURL)

	if outputRepoURL != expectedOutputRepoURL && outputLanguageURL != expectedOutputLanguageURL && outputContributorURL != expectedOutputContributorURL {
		t.Error("outputs does not match the expected result")
	}

}

func TestLanguageParser(t *testing.T) {
	var testInput map[string]interface{}
	testInput = make(map[string]interface{})
	testInput["Go"] = 123
	testInput["C++"] = 432
	expectedOutput := []string{"Go", "C++"}
	testOutput := LanguageParser(testInput)

	if testOutput[0] != expectedOutput[0] {
		t.Error("outputs does not match")
	}
}
