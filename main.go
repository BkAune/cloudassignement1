package main

import (
	"encoding/json"
	"net/http"
	"os"
	"strings"
)

//ClientResponsePayload the struct used to send JSON data to the client
type ClientResponsePayload struct {
	Project   string   `json:"project"`
	Owner     string   `json:"owner"`
	Commiter  string   `json:"committer"`
	Commits   int      `json:"commits"`
	Languages []string `json:"language"`
}

//ParseURL takes a string slice and builds the 3 urls needed for the api calls
func ParseURL(urlParts []string) (repoURL, languageURL, contributorURL string) {
	urlStartPart := "https://api.github.com/repos/"
	repoURL = urlStartPart + urlParts[4] + "/" + urlParts[5]
	languageURL = repoURL + "/languages"
	contributorURL = repoURL + "/contributors"
	return
}

//LanguageParser returns slice containing langues from repo
func LanguageParser(responseLanguage map[string]interface{}) (languages []string) {
	languages = make([]string, 0, len(responseLanguage))
	for key := range responseLanguage {
		languages = append(languages, key)
	}
	return
}

//ContributorParser returns main contributor and number of contributons
func ContributorParser(responseContributor interface{}) (contributorName string, castedContributions int) {
	contributors := responseContributor.([]interface{})
	highestContributor := contributors[0].(map[string]interface{})
	contributorName = highestContributor["login"].(string)
	contributions := highestContributor["contributions"].(float64) //this is stored as a float 64, which means we have to cast it to an int before pasing it to the struct
	castedContributions = int(contributions)
	return
}

//RepoParser returns name of repo and owner of repo
func RepoParser(responseRepository map[string]interface{}, w http.ResponseWriter) (repoName, repoOwnerName string) {
	repoName = responseRepository["name"].(string)
	repoOwner := responseRepository["owner"].(map[string]interface{})
	repoOwnerName = repoOwner["login"].(string)
	return
}

//HandlerURL takes parameters request and returns JSON objcet
func HandlerURL(w http.ResponseWriter, r *http.Request) {
	//adding header to the http.Resoponse to the client
	http.Header.Add(w.Header(), "content-type", "application/json")
	var responsePayload ClientResponsePayload
	//index 1 and 2 contains: "projectinfo" and "v1", as defined in the handlerFunc
	urlParts := strings.Split(r.URL.Path, "/")
	if urlParts[3] != "github.com" && len(urlParts) != 6 {
		http.Error(w, http.StatusText(400), 400)
	} else if urlParts[3] == "github.com" && len(urlParts) != 6 {
		http.Error(w, http.StatusText(400), 400)
	} else {
		urlRepository, urlLanguage, urlContributor := ParseURL(urlParts)
		//getting the 3 JSON responses from the REST api
		respRepository, conectErr := http.Get(urlRepository)
		respContributor, conectErr1 := http.Get(urlContributor)
		respLanguage, conectErr2 := http.Get(urlLanguage)
		//checking to see if there are any conection errors
		if conectErr != nil && conectErr1 != nil && conectErr2 != nil {
		} else {
			var responseRepository map[string]interface{}
			var responseLanguage map[string]interface{}
			var responseContributor interface{}
			//decoding the 3 https respones.
			decodeErr := json.NewDecoder(respRepository.Body).Decode(&responseRepository)
			decodeErr1 := json.NewDecoder(respContributor.Body).Decode(&responseContributor)
			decodeErr2 := json.NewDecoder(respLanguage.Body).Decode(&responseLanguage)
			if decodeErr != nil && decodeErr1 != nil && decodeErr2 != nil {
				//fmt.Fprintf(w, "decoder error")
			} else {
				//assigning the extracted variables to the response payload
				responsePayload.Project, responsePayload.Owner = RepoParser(responseRepository, w)
				responsePayload.Commiter, responsePayload.Commits = ContributorParser(responseContributor)
				responsePayload.Languages = LanguageParser(responseLanguage)
				//encoding the payloadStruct and sending it to the client
				json.NewEncoder(w).Encode(responsePayload)
			}
		}
	}
}

func main() {
	http.HandleFunc("/projectinfo/v1/", HandlerURL)
	http.ListenAndServe("0.0.0.0:"+os.Getenv("PORT"), nil)
}
